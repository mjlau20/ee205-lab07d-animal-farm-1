///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    main.c
/// @version 1.0
///
/// @author Michael Lau <mjlau20hawaii.edu>
/// @date   03_Mar_2022 
///////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include <stdlib.h>

int main() {
   printf( "Starting Animal Farm 1\n" );
   printf( "Done with Animal Farm 1\n" );

}
