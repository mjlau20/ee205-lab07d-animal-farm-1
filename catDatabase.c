///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    catDatabase.c
/// @version 1.0
///
/// @author Michael Lau <mjlau20hawaii.edu>
/// @date   03_Mar_2022 
///////////////////////////////////////////////////////////////////////////////
//

#include "catDatabase.h"

char  name[MAX_CATS][MAX_CAT_NAME];
float weight[MAX_CATS];
bool  isFixed[MAX_CATS];
enum  Gender gender[MAX_CATS];
enum  Breed breed[MAX_CATS];

// @todo initialize name
// int inThisYear[365];
//    for( int i = 0 ; i < 365 ; i++ ) inThisYear = 0;
