###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### @author Michael Lau <mjlau20hawaii.edu>
### @date   03_Mar_2022
###############################################################################

CC 	 = gcc
CFLAGS = -g -Wall -Wextra

TARGET = main

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c 

addCats.o: addCats.c catDatabase.o
	$(CC) $(CFLAGS) -c addCats.c catDatabase.o catDatabase.h 

test: main addCats.c
			./main
			./addCats
clean:
	rm -f $(TARGET) *.o
