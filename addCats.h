///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    addCats.h
/// @version 1.0
///
/// @author Michael Lau <mjlau20hawaii.edu>
/// @date   03_Mar_2022 
///////////////////////////////////////////////////////////////////////////////
//

#pragma once

#include <string.h>
#include "catDatabase.h"

int addCat( const char newName[], const enum Gender newGender, const enum Breed newBreed,
            const bool newIsFixed, const float newWeight );

